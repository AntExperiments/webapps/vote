import type { NextApiRequest, NextApiResponse } from 'next'

var polls = require('fs').readdirSync('public/polls').map((x: string) => { return {
  filename: x.split('.yaml')[0]
}});

interface Data {
  polls: [{
    filename: string
  }]
}

const Handler = (req: NextApiRequest, res: NextApiResponse<Data>) => {
  res.status(200).json( { polls } )
}

export default Handler