import type { NextPage } from 'next'
import styles from '../styles/Home.module.scss'
import YAML from 'yaml'
interface Question {
  type: string;
  name: string;
  options?: [string];
}
interface Poll {
  title: string;
  sections: [Question]
}
interface Props {
  data: Poll;
}

const Home: NextPage<Props> = ({ data }) =>
  data == undefined
  
  && (
    <div className={styles.container}>
      <span>Could not find this poll</span>
    </div>
  )

  || (
    <div className={styles.container}>
      {() => {
        if (data == undefined) {
          <span>Could not load</span>
        } else {
          
        }
      }}
      <h1 className={styles.title}>
        {data.title}
      </h1>
      <div className={styles.questionsContainer}>

        {data.sections.map(x =>
          <div className={styles.inputGroup} key={x.name}>
            <span>{x.name}</span>

            {"select" == x.type && (
              <select name="" id="">
                {x.options?.map(y =>
                  <option key={y} value={y}>{y}</option>
                )}
              </select>
            )}

            {"checkbox" == x.type && x.options?.map(y =>
              <label key={y}>
                <input type="checkbox" name={x.name} />{y}
              </label>
            )}

            {"radio" == x.type && x.options?.map(y =>
              <label key={y}>
                <input type="radio" name={x.name} />{y}
              </label>
            )}

            {"textarea" == x.type && (
              <textarea name={x.name} id="" rows={4}></textarea>
            )}

          </div>
        )}

      </div>

      <div className={styles.submitArea}>
        <input type="text" name="submitter_name" placeholder="Your Name (optional)" />
        <input type="email" name="submitter_email" placeholder="Your Email (optional)" />
        <a href="" className="btn">
          <svg viewBox="0 0 24 24">
              <path fill="currentColor" d="M15,9H5V5H15M12,19A3,3 0 0,1 9,16A3,3 0 0,1 12,13A3,3 0 0,1 15,16A3,3 0 0,1 12,19M17,3H5C3.89,3 3,3.9 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V7L17,3Z" />
          </svg>
        </a>
      </div>
    </div>
  )


Home.getInitialProps = async ({ query }) => {
  const yaml: Poll = await fetch(`http://localhost:3000/polls/${query.id}.yaml`)
  .then(res => res.text())
  .then(text => YAML.parse(text))
  .catch(() => undefined)
  return { data: yaml }
}

export default Home
