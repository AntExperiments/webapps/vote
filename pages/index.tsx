import type { NextPage } from 'next'
import styles from '../styles/Home.module.scss'

interface Props {
  data: [{
    filename: string
  }]
}

const Home: NextPage<Props> = ({ data }) => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>AntVotes</h1>

      <ul>
        {data.map(p => 
          <li>
            <a href={`/${p.filename}`}> {p.filename} </a>
          </li>
        )}
      </ul>
    </div>
  )
}

Home.getInitialProps = async ctx => {
  const res = await fetch('http://localhost:3000/api');
  const content = await res.json()
  return { data: content["polls"] }
}

export default Home
